/* =============================================================================
**  Thisf file is part of the parmmg software package for the tetrahedral
**  mesh modification.
**  Copyright (c) Bx INP/Inria/UBordeaux/InriaSoft, 2017- .
**
**  parmmg is free software: you can redistribute it and/or modify it
**  under the terms of the GNU Lesser General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  parmmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License and of the GNU General Public License along with parmmg (in
**  files COPYING.LESSER and COPYING). If not, see
**  <http://www.gnu.org/licenses/>. Please read their terms carefully and
**  use this copy of the parmmg distribution only if you accept them.
** =============================================================================
*/

/**
 * Unitary test of graph functions.
 *
 * \author Luca Cirrottola (Inria)
 *
 * \copyright GNU Lesser General Public License.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libparmmg.h"
#include "metis_pmmg.h"
#include "linkedlist_pmmg.h"

#define nvtxs_all 14
#define nadjncy_all 58

int main(int argc,char *argv[]) {
  PMMG_pParMesh     parmesh;
  PMMG_graph graph,graph_seq;
  int               k,i,nitem,new_nitem,nitem2comm,idx,*array;
  int               list_size,ier;

  MPI_Init( &argc,&argv );

  parmesh = NULL;
  ier = PMMG_Init_parMesh( PMMG_ARG_start,
                           PMMG_ARG_ppParMesh,&parmesh,
                           PMMG_ARG_dim,3,
                           PMMG_ARG_MPIComm,MPI_COMM_WORLD,
                           PMMG_ARG_end );

  if ( ier!=1 ) return 1;

  /* centralized graph */
  int xadj_all[nvtxs_all+1] = {0,3,6,11,15,20,26,29,34,39,41,47,51,55,58};
  int adjncy_all[nadjncy_all+1] = { 1, 2, 3,
                                    0, 2, 4,
                                    0, 1, 4, 5, 3,
                                    0, 2, 5, 8,
                                    1, 2, 5, 7, 6,
                                    2, 4, 7,10, 8, 3,
                                    4, 7,12,
                                    4, 6,12,10, 5,
                                    3, 5,10,11, 9,
                                    8,11,
                                    5, 7,12,13,11, 8,
                                    9, 8,10,13,
                                    7, 6,13,10,
                                   10,12,11};

  double coords[nvtxs_all][2] = {{0.10,1.00},
                                 {0.00,0.70},
                                 {0.20,0.70},
                                 {0.50,0.90},
                                 {0.10,0.40},
                                 {0.45,0.60},
                                 {0.10,0.00},
                                 {0.30,0.20},
                                 {0.70,0.80},
                                 {1.00,0.60},
                                 {0.60,0.30},
                                 {0.90,0.20},
                                 {0.50,0.00},
                                 {0.80,0.00}};

  int color[nvtxs_all] = {0,0,0,1,0,1,1,1,1,0,1,0,1,0};
  int vtxdist[3] = {0, 8, nvtxs_all};
  int vwgt_all[nvtxs_all],adjwgt_all[nadjncy_all];
  for( int i = 0; i < nvtxs_all; i++ )
    vwgt_all[i] = 1;
  for( int i = 0; i < nadjncy_all; i++ )
    adjwgt_all[i] = 1;

  /* Create map from centralized to reduced graph */
  int map_all[nvtxs_all] = {-1, -1, -1,  0, -1,  1,  5,
                             3,  2, -1,  4, -1,  6, -1};

//  PMMG_graph_init( parmesh, &graph );
//  PMMG_graph_set( parmesh, &graph, nvtxs_all, nadjncy_all, xadj_all, adjncy_all, vtxdist );
//  PMMG_graph_save( parmesh, &graph, color, 2, coords );
//  PMMG_graph_free( parmesh, &graph );

  /* partition graph */
  int nvtxs = vtxdist[parmesh->myrank+1]-vtxdist[parmesh->myrank];
  int nadjncy = xadj_all[vtxdist[parmesh->myrank+1]] - xadj_all[vtxdist[parmesh->myrank]];
  int xadj[nvtxs+1];
  int adjncy[nadjncy+1];
  int map[nvtxs];

  int ivstart,xadjstart;
  xadj[0] = 0;
  ivstart = vtxdist[parmesh->myrank];
  xadjstart = xadj_all[ivstart];

  xadj[0] = 0;
  for( int ivtx = 0; ivtx < nvtxs; ivtx++ ) {
    xadj[ivtx+1] = xadj_all[ivstart+ivtx+1]-xadjstart;
    for( int j = 0; j < xadj[ivtx+1]-xadj[ivtx]; j++ ) {
      adjncy[xadj[ivtx]+j] = adjncy_all[xadj_all[ivstart+ivtx]+j];
    }
    map[ivtx] = map_all[ivstart+ivtx];
  }

  PMMG_graph_init( parmesh, &graph );
  ier = PMMG_graph_set( parmesh, &graph, nvtxs, nadjncy, xadj, adjncy,
                        vwgt_all, adjwgt_all, vtxdist, map );

  /** Gather the graph on proc 0 */
  int root = 0;
  if( !PMMG_graph_gather( parmesh, &graph, &graph_seq, root ) )
    return 0;

  if( parmesh->myrank == root ) {
    /* Save centralized graph */
    PMMG_graph_save( parmesh, &graph_seq, color, 2, coords, NULL );

    PMMG_HGrp  hash;
    PMMG_graph subgraph;

    /* Initialize subgraph and hash table */
    if( !PMMG_subgraph_init( parmesh, &subgraph, nvtxs_all, nadjncy_all ) )
      return 0;
    /* hash is used to store the sorted list of adjacent groups to a group */
    if ( !PMMG_hashNew( parmesh,&hash,graph_seq.nvtxs+1,
                        PMMG_NBADJA_GRPS*graph_seq.nvtxs+1) )
      return 0;


    subgraph.nvtxs = 7;
    subgraph.npart = 2;

    if( !PMMG_subgraph_build( parmesh, &graph_seq, &subgraph, &hash ) )
      return 0;
    /* Save graph */
    PMMG_graph_save( parmesh, &subgraph, color, 2, coords, subgraph.map );

    /* Allocate the partition array, sized with the maximum number
     * of active nodes (reuse it for the active subgraph and the collapsed graph */
    int part_sub[nvtxs_all],part_seq[nvtxs_all];

     if( !PMMG_subgraph_part( parmesh, &subgraph, part_sub ) )
      return 0;

    /* Collapse map using the partition array, count inactive nodes */
    if( !PMMG_subgraph_map_collapse( parmesh,&graph_seq,&subgraph,part_sub,
                                     root ) )
      return 0;

    /* reset hash */
    PMMG_hashReset( parmesh,&hash );

    /* extract collapsed graph */
    if( !PMMG_subgraph_build( parmesh, &graph_seq, &subgraph, &hash ) )
      return 0;

    /* Save graph */
    PMMG_graph_save( parmesh, &subgraph, color, 2, coords, subgraph.map );

    /* Partition the collapsed graph */
    if( !PMMG_subgraph_part( parmesh, &subgraph, part_sub ) )
      return 0;

    /* Update the original partition array */
    PMMG_graph_map_subpart( parmesh, &graph_seq, part_seq, part_sub );
//    PMMG_graph_save( parmesh, &graph_seq, part_seq, 2, coords, NULL );

  }

  /* Free */
  if( parmesh->myrank == root ) {
    PMMG_graph_free( parmesh,&graph_seq );
  }
  PMMG_graph_free( parmesh, &graph );



  ier = PMMG_Free_all( PMMG_ARG_start,
                       PMMG_ARG_ppParMesh,&parmesh,
                       PMMG_ARG_end );

  MPI_Finalize();
  if ( !ier ) return 1;

  return 0;
}
