/* =============================================================================
**  Thisf file is part of the parmmg software package for the tetrahedral
**  mesh modification.
**  Copyright (c) Bx INP/Inria/UBordeaux/InriaSoft, 2017- .
**
**  parmmg is free software: you can redistribute it and/or modify it
**  under the terms of the GNU Lesser General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  parmmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License and of the GNU General Public License along with parmmg (in
**  files COPYING.LESSER and COPYING). If not, see
**  <http://www.gnu.org/licenses/>. Please read their terms carefully and
**  use this copy of the parmmg distribution only if you accept them.
** =============================================================================
*/

/**
 * Unitary test of the functions listed in the \a linkedList_pmmg.c
 * file.
 *
 * \author Algiane Froehly (InriaSoft)
 *
 * \copyright GNU Lesser General Public License.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "libparmmg.h"
#include "linkedlist_pmmg.h"

int main(int argc,char *argv[]) {
  PMMG_pParMesh     parmesh;
  PMMG_cellLnkdList **proclists;
  int               k,i,nitem,new_nitem,nitem2comm,idx,*array;
  int               list_size,ier;

  ier = 1;

  parmesh = NULL;
  ier = PMMG_Init_parMesh( PMMG_ARG_start,
                           PMMG_ARG_ppParMesh,&parmesh,
                           PMMG_ARG_dim,3,
                           PMMG_ARG_MPIComm,MPI_COMM_WORLD,
                           PMMG_ARG_end );

  if ( ier!=1 ) return 10;

  list_size = 5;
  nitem = 10;
  proclists = NULL;
  PMMG_CALLOC(parmesh,proclists,nitem,PMMG_cellLnkdList*,"array of linked lists",
              goto end);

  for ( k = 0; k<nitem; ++k ) {
    PMMG_MALLOC(parmesh,proclists[k],1,PMMG_cellLnkdList,"linked list",goto end);
    if ( !PMMG_cellLnkdListNew(parmesh,proclists[k],k,list_size) ) goto end;
  }

  /** Add manually items inside different lists */
  /* list 0 */
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[0],1,2 )) return 9;
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[0],1,1 )) return 9;
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[0],1,2 )) return 9;
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[0],2,3 )) return 9;

  /* list 2 */
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[2],3,1 )) return 7;
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[2],1,2 )) return 7;

  /* list 3 */
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[3],1,2 )) return 8;
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[3],4,5 )) return 8;
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[3],4,1 )) return 8;
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[3],3,1 )) return 8;

  /* list 6 */
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[6],3,1 )) return 11;
  if ( !PMMG_add_cell2lnkdList( parmesh,proclists[6],1,2 )) return 11;

  printf("\n --------- Print initial lists\n");
  for ( k = 0; k<nitem; ++k ) {
    printf("    list[%d]: \n",k);
    PMMG_print_cellLnkdList( proclists[k] );
  }

  printf("\n --------- Test merge function\n");
  puts("Merge 6->4");
  if ( !PMMG_merge_cellLnkdList(parmesh,proclists[4],proclists[6]) ) return 6;
  printf("    list[4]: \n");
  PMMG_print_cellLnkdList( proclists[4] );

  puts("Merge 5->4");
  printf("    list[4]: \n");
  if ( !PMMG_merge_cellLnkdList(parmesh,proclists[4],proclists[5]) ) return 5;
  PMMG_print_cellLnkdList( proclists[4] );

  puts("Merge 6->4");
  printf("    list[4]: \n");
  if ( 2!=PMMG_merge_cellLnkdList(parmesh,proclists[4],proclists[6]) ) return 4;
  PMMG_print_cellLnkdList( proclists[4] );

  puts("Merge 2->0");
  printf("    list[0]: \n");
  if ( !PMMG_merge_cellLnkdList(parmesh,proclists[0],proclists[2]) ) return 3;
  PMMG_print_cellLnkdList( proclists[0] );

  printf("\n --------- Test pack/unpack functions\n");
  nitem2comm = 0;
  for ( k = 0; k<nitem; ++k ) {
      nitem2comm += proclists[k]->nitem*2+1;
  }
  PMMG_CALLOC(parmesh,array,nitem2comm,int,"array of linked lists",
              goto end);

  printf("    pack list[0] and list[2]: \n");
  idx  = PMMG_packInArray_cellLnkdList( proclists[0], &array[0] );
  PMMG_packInArray_cellLnkdList( proclists[2], &array[idx] );

  printf("    unpack in list[8] and list[9]: \n");
  idx  = PMMG_unpackArray_inCellLnkdList( parmesh, proclists[8],&array[0] );
  PMMG_unpackArray_inCellLnkdList( parmesh, proclists[9],&array[idx] );

  if ( PMMG_compare_cellLnkdList(&proclists[0],&proclists[8]) ) return 1;
  if ( PMMG_compare_cellLnkdList(&proclists[2],&proclists[9]) ) return 1;

  printf("    list[8]: \n");
  PMMG_print_cellLnkdList( proclists[8] );
  printf("    list[9]: \n");
  PMMG_print_cellLnkdList( proclists[9] );

  printf("\n --------- Delete empty lists\n");
  idx = 0;
  for ( i=0; i<nitem; ++i ) {
    if (!proclists[i]->nitem) {
      continue;
    }
    else if ( idx != i ) {
      proclists[idx] = proclists[i];
    }
    idx++;
  }
  new_nitem = idx;

  for ( k = 0; k<new_nitem; ++k ) {
    assert(proclists[k]->nitem);
    printf("    list[%d]: \n",k);
    PMMG_print_cellLnkdList( proclists[k] );
  }

  printf("\n --------- Sort the lists using qsort\n");
  qsort(proclists, new_nitem, sizeof(PMMG_cellLnkdList*),PMMG_compare_cellLnkdList);

  for ( k = 0; k<new_nitem; ++k ) {
    for ( i = k+1; i<new_nitem; ++i ) {
      if ( PMMG_compare_cellLnkdList(&proclists[i],&proclists[k]) < 0 ) return 2;
    }
  }

  for ( k = 0; k<new_nitem; ++k ) {
    printf("    list[%d]: \n",k);
    PMMG_print_cellLnkdList( proclists[k] );
  }

  printf("\n --------- Doublon deletion\n");
  idx = 0;
  for ( i=1; i<new_nitem; ++i ) {
    if ( PMMG_compare_cellLnkdList(&proclists[i],&proclists[idx]) ) {
      ++idx;
      if ( idx != i ) {
        proclists[idx] = proclists[i];
      }
    }
  }
  new_nitem = idx+1;

  for ( k = 0; k<new_nitem; ++k ) {
    for ( i = k+1; i<new_nitem; ++i ) {
      ier = PMMG_compare_cellLnkdList(&proclists[i],&proclists[k]);
      if ( !ier ) return 2;
    }
  }

  for ( k = 0; k<new_nitem; ++k ) {
    printf("    list[%d]: \n",k);
    PMMG_print_cellLnkdList( proclists[k] );
  }
  /* Success */
  ier = 0;

end:
  for ( k = 0; k<nitem; ++k ) {
    PMMG_DEL_MEM(parmesh,proclists[k]->item,PMMG_lnkdCell,"linked list");
  }
  PMMG_DEL_MEM(parmesh,proclists,PMMG_cellLnkdList*,"array of linked lists");

  ier = PMMG_Free_all( PMMG_ARG_start,
                       PMMG_ARG_ppParMesh,&parmesh,
                       PMMG_ARG_end );

  if ( !ier ) return 2;

  return 0;
}
