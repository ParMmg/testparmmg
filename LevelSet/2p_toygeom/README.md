# Remark
In multimat mode, Mmg fail to insert points along boundary ls and old material edges because they are non-manifold edges with 2 singular extremities: it is normal for now. A more physical test case would give better adp results.

# Debug history

## commit `d09630a42ca67fef441e` (branch `feature/edge-tag-consistency`)
   - Command line :
```
     mpirun -n 2 parmmg_debug $TEST_PATH/testparmmg/LevelSet/2p_toygeom/cube-distributed-nodes-nomat-edges.mesh "-v" "5" "-hsiz" "0.1" "-ls" "0.0" "-sol $TEST_PATH/testparmmg/LevelSet/2p_toygeom/cube-ls.sol"
```

   - Bug description: after ls discretization, on rank 0, the point 11 (that has
     been added by the level-set splitting) is not marked as `MG_REF`
     while edges 8 - 11 and 11 - 7 are marked `MG_REF`.

   - Investigation:
       - during // level-set insertion, new points are created without
         tags because we travel the external comm of edges ot insert
         new nodes and we don't store the edge tag.

       - after this step, the tags along // edges are analyzed again
         by the `PMMG_setfeatures` function but:
         - the edge 8-11 (for example) is not detected as a `MG_REF`
           edge as it doesn't belongs to triangles with different
           references (all triangles have a 0 ref)
         - this same edge is not non-manifold (it doesn't belong to
           the level-set so it belongs to only 2 triangles if we
           ignore `MG_PARBDY` triangles that are not marked as
           `MG_PARBDYBDY`)
         - edge `15-11` (for example) is not marked either as
           non-manifold as `MG_PARBDY` triangles that are not
           `MG_PARBDYBDY` are not considered as true surfaces.
         - thus, finally, the `setfeatures` function doesn't modify
           the point xstag

        - in Mmg, the `MMG5_setadj` function (`analys_3d.c`) assign
          the triangle edge tag to points but not for edges at
          interfave between a physical boundary and a parallel
          interface (l145 and onwards)

   - Solution: commit `0b1afdc37e66e2d93ae594c93f9752a7ab0311c3`
     proposes to store the edge tag in the point tag at the end of the
     ``PMMG_setfeatures` function. The code used in `MMG5_setadj` is
     directly copied. It is possible that wwe transfer unwanted tag
     from the edge to the point. Also, I am nos sure for the treatment
     of the `NOSURF` tag when the point is already required (maybe the
     required tag has been added by ParMmg itself here...).
