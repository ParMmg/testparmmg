/**
 * Test PMMG_Set_*name API functions
 *
 * \author Algiane Froehly (Inria)
 * \version 1
 * \copyright GNU Lesser General Public License.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MIN(a,b) (((a) < (b)) ? (a) : (b))

/** Include the parmmg library hader file */
// if the header file is in the "include" directory
// #include "libparmmg.h"
// if the header file is in "include/parmmg"
#include "parmmg/libparmmg.h"

int main(int argc,char *argv[]) {
  PMMG_pParMesh   parmesh;
  int             ier,err,rank;


  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );

  if ( !rank ) fprintf(stdout,"  -- TEST PARMMGLIB - PMMG_Set_Xname API \n");

  /** ------------------------------ STEP   I -------------------------- */
  /** 1) Initialisation of th parmesh structures */
  /* args of InitMesh:
   * PMMG_ARG_start: we start to give the args of a variadic func
   * PMMG_ARG_ppParMesh: next arg will be a pointer over a PMMG_pParMesh
   * &parmesh: pointer toward your PMMG_pParMesh
   * MMG5_ARG_pMesh: initialization of a mesh inside the parmesh.
   * MMG5_ARG_pMet: init a metric inside the parmesh
   * PMMG_ARG_dim: next arg will be the mesh dimension
   * 3: mesh dimension
   * PMMG_MPIComm: next arg will be the MPI COmmunicator
   * MPI_COMM_WORLD: MPI communicator
   *
   */
  parmesh = NULL;

  PMMG_Init_parMesh(PMMG_ARG_start,
                    PMMG_ARG_ppParMesh,&parmesh,
                    PMMG_ARG_pMesh,PMMG_ARG_pMet,PMMG_ARG_pLs,
                    PMMG_ARG_pDisp,
                    PMMG_ARG_dim,3,PMMG_ARG_MPIComm,MPI_COMM_WORLD,
                    PMMG_ARG_end);


  /* Test default metric setting without meshname */
  ier = PMMG_Set_inputMetName(parmesh, "");
  err = PMMG_Set_outputMetName(parmesh, "");

  printf("\nDefault metric setting without mesh name: %s %s\n",parmesh->metin,parmesh->metout);
  ier = MIN(ier,err);

  /* Test default meshname */
  err = PMMG_Set_inputMeshName(parmesh, "");
  ier = MIN(ier,err);
  err = PMMG_Set_outputMeshName(parmesh, "");
  ier = MIN(ier,err);

  printf("Default mesh name: %s %s\n",parmesh->meshin,parmesh->meshout);

  /* Test default metric setting with default meshname */
  err = PMMG_Set_inputMetName(parmesh, "");
  ier = MIN(ier,err);
  err = PMMG_Set_outputMetName(parmesh, "");
  ier = MIN(ier,err);

  printf("Default metric setting with default mesh name:  %s %s\n",parmesh->metin,parmesh->metout);

 /* Test default ls setting with default meshname */
  err = PMMG_Set_inputLsName(parmesh, "");
  ier = MIN(ier,err);

  printf("Default ls setting with default mesh name:  %s\n",parmesh->lsin);

 /* Test default disp setting with default meshname */
  err = PMMG_Set_inputDispName(parmesh, "");
  ier = MIN(ier,err);

  printf("Default disp setting with default mesh name:  %s\n",parmesh->dispin);


  /* Test meshname setting */
  err = PMMG_Set_inputMeshName(parmesh, "toto.mesh");
  ier = MIN(ier,err);
  err = PMMG_Set_outputMeshName(parmesh, "titi.mesh");
  ier = MIN(ier,err);

  printf("Default mesh name %s %s\n",parmesh->meshin,parmesh->meshout);

  /* Test default metric setting with setted meshname */
  err = PMMG_Set_inputMetName(parmesh, "");
  ier = MIN(ier,err);
  err = PMMG_Set_outputMetName(parmesh, "");
  ier = MIN(ier,err);
  printf("Default metric name setting with mesh name: %s %s\n",parmesh->metin,parmesh->metout);

  /* Test default ls setting with setted meshname */
  err = PMMG_Set_inputLsName(parmesh, "");
  ier = MIN(ier,err);
  printf("Default ls name setting with mesh name: %s\n",parmesh->lsin);

  /* Test default disp setting with setted meshname */
  err = PMMG_Set_inputDispName(parmesh, "");
  ier = MIN(ier,err);
  printf("Default disp name setting with mesh name: %s\n",parmesh->dispin);

  /* Test tricky meshname setting (dot in path + .sol extension for the mesh) */
  err = PMMG_Set_inputMeshName(parmesh, "input.path/tricky.sol");
  ier = MIN(ier,err);
  err = PMMG_Set_outputMeshName(parmesh, "output.path/tricky.o");
  ier = MIN(ier,err);

  printf("Default mesh name %s %s\n",parmesh->meshin,parmesh->meshout);

  /* Test default metric setting with tricky meshname */
  err = PMMG_Set_inputMetName(parmesh, "");
  ier = MIN(ier,err);
  err = PMMG_Set_outputMetName(parmesh, "");
  ier = MIN(ier,err);
  printf("Default metric name setting with tricky mesh name: %s %s\n",parmesh->metin,parmesh->metout);

  /* Test metric setting */
  err = PMMG_Set_inputMetName(parmesh, "toto/metric.sol");
  ier = MIN(ier,err);
  err = PMMG_Set_outputMetName(parmesh, "titimetric");
  ier = MIN(ier,err);
  printf("Metric name setting with mesh name: %s %s\n",parmesh->metin,parmesh->metout);

  /* Test input field setting */
  err = PMMG_Set_inputSolsName(parmesh, "totofield");
  ier = MIN(ier,err);
  /* Test default output field setting */
  err = PMMG_Set_outputSolsName(parmesh, "");
  ier = MIN(ier,err);

  printf("Input field name setting and default output field name:  %s %s\n",parmesh->fieldin,parmesh->fieldout);

  /* Test default output field setting */
  err = PMMG_Set_outputSolsName(parmesh, "titifield");
  ier = MIN(ier,err);
  printf("Output field name setting:  %s\n",parmesh->fieldout);

  PMMG_Free_all(PMMG_ARG_start,
                PMMG_ARG_ppParMesh,&parmesh,
                PMMG_ARG_end);

  /* Return 0 (success) if ier==1 (mmg API success)
     Return 1 (failure) if ier==0 (mmg API failure) */
  return (ier==0);
}
