/**
 * \author Luca Cirrottola (Inria)
 * \version 1
 * \copyright GNU Lesser General Public License.
 */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <float.h>

#include "libparmmg.h"
#include "parmmg.h"
#include "locate_pmmg.h"

int main(int argc,char *argv[]) {
  PMMG_pParMesh   parmesh;
  char            *filein,*fileout;
  int             rank,nprocs;

  MPI_Init( &argc, &argv );
  MPI_Comm_rank( MPI_COMM_WORLD, &rank );
  MPI_Comm_size( MPI_COMM_WORLD, &nprocs );

  if ( (argc!=7) && !rank ) {
    printf(" Usage: %s filein fileout\n",argv[0]);
    return 1;
  }

  /* Name and path of the input mesh file */
  filein = (char *) calloc(strlen(argv[1]) + 1, sizeof(char));
  if ( filein == NULL ) {
    perror("  ## Memory problem: calloc");
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }
  strcpy(filein,argv[1]);

  /* Name and path of the input mesh file */
  fileout = (char *) calloc(strlen(argv[2]) + 1, sizeof(char));
  if ( fileout == NULL ) {
    perror("  ## Memory problem: calloc");
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }
  strcpy(fileout,argv[2]);


  /** 1) Initialisation of th parmesh structures */
  parmesh = NULL;

  PMMG_Init_parMesh(PMMG_ARG_start,
                    PMMG_ARG_ppParMesh,&parmesh,
                    PMMG_ARG_pMesh,PMMG_ARG_pMet,
                    PMMG_ARG_dim,3,PMMG_ARG_MPIComm,MPI_COMM_WORLD,
                    PMMG_ARG_end);


  /** 2) Build mesh in MMG5 format */
  if ( PMMG_loadMesh_centralized(parmesh,filein) != 1 ) {
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }

  int np,ne,nprism,nt,nquad,na;
  if ( PMMG_Get_meshSize(parmesh,&np,&ne,&nprism,&nt,&nquad,&na) != 1 ) {
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }


  /**
   *  3) Locate points in the mesh
   */

  if ( PMMG_Set_iparameter(parmesh,PMMG_IPARAM_verbose,10) != 1 ) {
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }

  MMG5_pMesh     mesh;
  MMG5_pTria     ptr;
  MMG5_pPoint    ppt;
  PMMG_barycoord barycoord[4];
  double         *triaNormals;
  int            *nodeTrias;
  int            k,ip,ifoundTria,ifoundEdge,ifoundVertex;
  MMG5_Hash      hash;
  int            ier;

  mesh = parmesh->listgrp[0].mesh;

  /* Create boundary */
  MMG3D_hashTetra(mesh,0);
  if ( !MMG5_chkBdryTria(mesh) ) {
    fprintf(stderr,"\n  ## Problem building boundary.\n");
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }

  /* Create surface adjacency */
  memset ( &hash, 0x0, sizeof(MMG5_Hash));
  if ( !MMG3D_hashTria(mesh,&hash) ) {
    MMG5_DEL_MEM(mesh,hash.item);
    fprintf(stderr,"\n  ## Hashing problem.\n");
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }
  MMG5_DEL_MEM(mesh,hash.item);

  /* Compute triangle normals */
  triaNormals = (double*)malloc(3*(nt+1)*sizeof(double));
  PMMG_precompute_triaNormals( mesh,triaNormals);

  /* Compute node tris graph */
  PMMG_precompute_nodeTrias( parmesh,mesh,&nodeTrias );

  /* Use point 0 to store the point to locate */
  ip = 0;
  ppt = &mesh->point[ip];

  /* Search a saddle point */
  ppt->c[0] = atof(argv[3]);
  ppt->c[1] = atof(argv[4]);
  ppt->c[2] = atof(argv[5]);

  ifoundTria = atoi(argv[6]);
  ier = PMMG_locatePointBdy( mesh, ppt,
                             triaNormals, nodeTrias, barycoord,
                             &ifoundTria, &ifoundEdge, &ifoundVertex );

  PMMG_locatePoint_errorCheck( mesh,ip,ier,rank,0 );

  /* Exit with error if not found */
  if( !ier ) {
    MPI_Finalize();
    exit(EXIT_FAILURE);
  }
  printf("Found point in triangle %d.\n",ifoundTria);
  if( ifoundVertex >= 0 ) printf("Found point in cone.\n");
  if( ifoundEdge >= 0 )   printf("Found point in wedge.\n");


  /* Save paths */
  for( k = 1; k <= mesh->nt; k++ ) {
    ptr = &mesh->tria[k];
    ptr->ref = ptr->flag;
  }
  ptr = &mesh->tria[ifoundTria];
  ptr->ref = ptr->flag+1;

  ppt = &mesh->point[++(mesh->np)];
  for( k = 0; k < 3; k++ )
    ppt->c[k] = mesh->point[0].c[k];
  ppt->tag &= MG_REQ;
  ppt->tag &= MG_CRN;

  ppt = &mesh->point[++(mesh->np)];
  for( k = 0; k < 3; k++ )
    ppt->c[k] = mesh->point[0].c[k];
  ppt->tag &= MG_REQ;
  ppt->tag &= MG_CRN;

  ppt = &mesh->point[++(mesh->np)];
  for( k = 0; k < 3; k++ )
    ppt->c[k] = mesh->point[0].c[k]+k*0.01;
  ppt->tag &= MG_REQ;
  ppt->tag &= MG_CRN;

  printf("Locate point saved as %d\n",mesh->np);
  MMG5_SAFE_RECALLOC(mesh->tria,mesh->nt+1,mesh->nt+2,MMG5_Tria,"add tria",return EXIT_FAILURE);
  ptr = &mesh->tria[++(mesh->nt)];
  for( k = 0; k < 3; k++ )
    ptr->v[k] = mesh->np-k;
  ptr->ref = mesh->tria[ifoundTria].ref+1;

  MMG3D_saveMesh( mesh,fileout );


  /** 4) Free the PMMG5 structures */
  free(filein);
  free(fileout);
  free(triaNormals);
  PMMG_DEL_MEM(parmesh,nodeTrias,int,"nodeTrias");
  PMMG_Free_all(PMMG_ARG_start,
                PMMG_ARG_ppParMesh,&parmesh,
                PMMG_ARG_end);

  MPI_Finalize();

  return EXIT_SUCCESS;
}
