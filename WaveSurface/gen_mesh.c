/**
 * Example of call from a C code of the hex2tet library (convert an
 * array of hexa into a MMG5 tetrahedral mesh) and mmg3d library to discretize
 * a level set function on that mesh.
 *
 * Based on the library examples:
 * hex2tet/libexamples/C/ex1.c
 * mmg3/libexamples/mmg3d/IsosurfDiscretization_example0/main.c
 *
 * \author Luca Cirrottola (Inria)
 * \version 1
 * \copyright GNU Lesser General Public License.
 */

#include "hex2tet/libhex2tet.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <memory.h>
#include <string.h>
#include <math.h>

int main ( int argc, char* argv[] ) {

  /* Should be multiple of 4 times the wave length*/
  const int Width  = 20;
  const int Height = 20;
  const int Depth  = 20;

  MMG5_pMesh mmgMesh = NULL;
  MMG5_pSol  mmgSol  = NULL;

  int        ier;
  char       *fileout;

  fprintf(stdout,"  -- HEX2TET EXAMPLE \n");

  if ( argc != 2 ) {
    printf(" Usage: %s fileout \n",argv[0]);
    return 1;
  }

  /* Name and path of the output mesh file */
  fileout = (char *) calloc(strlen(argv[1]) + 1, sizeof(char));
  if ( fileout == NULL ) {
    perror("  ## Memory problem: calloc");
    exit(EXIT_FAILURE);
  }
  strcpy(fileout,argv[1]);

  /** Step 1: MMG5 mesh allocation */
  int nbVertices = (Width + 1)*(Height+1)*(Depth+1);
  int nbHex = (Width)    * (Height) * (Depth);

  MMG3D_Init_mesh(MMG5_ARG_start,
                  MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
                  MMG5_ARG_end);

  /** Step 2: Set the mesh size by giving the number of vertices and
   * the number of hexa of the hexahedral mesh */
  if ( H2T_Set_meshSize(mmgMesh, nbVertices, nbHex, 0, 0) != 1)
    exit(EXIT_FAILURE);

  /** Step 3: Give the mesh vertices to Hex2tet (hexahedra vertices) */
  int imax = Width+1;
  int jmax = Height+1;
  int kmax = Depth+1;
  double z0 = 0.5;
  double A = 0.25;
  int kx = 2;
  int ky = 2;
  int isTop,isBottom,isSaddle;

  int ref = 0;

  int *reqTab;
  reqTab = (int*)malloc((nbVertices+1)*sizeof(int));


  for ( int k=0; k<kmax; ++k ) {
    for ( int j=0; j<jmax; ++j ) {
      for ( int i=0; i<imax; ++i ) {
        /* Vertices insertion */
        int pointNumber = (k*jmax*imax) + (j*imax) + i + 1;

        /* Top peaks, bottom peaks, saddle points */
        isTop    = ( k == 3*Depth/4 ) &&
                   ( ( !( i%(Width/kx) ) && !( j%(Height/ky) ) ) ||
                     ( !( (i-Width/2/kx)%(Width/kx) ) && !( (j-Height/2/ky) % (Height/ky) ) ) );
        isBottom = ( k == Depth/4 ) &&
                   ( ( !( (i-Width/2/kx)%(Width/kx) ) && !( j%(Height/ky) ) ) ||
                     ( !( i%(Width/kx) ) && !( (j-Height/2/ky)%(Height/ky) ) ) );
        isSaddle = ( k == Depth/2 ) &&
                   ( !( (i-(Width/4/kx))%(Width/2/kx)) && !( (j-(Height/4/ky))%(Height/2/ky) ) );
        if( isTop || isBottom || isSaddle )
          reqTab[pointNumber] = 1;
        else
          reqTab[pointNumber] = 0;

        double v[3];
        v[0] = (double)i;
        v[1] = (double)j;
        v[2] = (double)k;
        v[0] *= 1.0/((double) Width);
        v[1] *= 1.0/((double) Height);
        v[2] *= 1.0/((double) Depth);
        if ( H2T_Set_vertex(mmgMesh, v[0], v[1], v[2], ref, pointNumber) != 1)
          exit ( EXIT_FAILURE );
      }
    }
  }

  /** Step 4: Fill the array of the hexa connectivity (hexTab) such as:
   * hexTab [ 9*k + i] is the vertex number i of the k^th hexahedra
   * (for k from 1 to nbHex and i from 0 to 7) and  hexTab [ 9*k + 8] is the
   * reference of the k^th hexa */
  int *hexTab;
  hexTab = (int*)malloc(9*(nbHex+1)*sizeof(int));

  for ( int k=0; k<kmax-1; ++k ) {
    for ( int j=0; j<jmax-1; ++j ) {
      for ( int i=0; i<imax-1; ++i ) {
        int hexaNumber     = (k*(jmax-1)*(imax-1)) + (j*(imax-1)) + i + 1;
        int hexTabPosition = 9 * hexaNumber;
        /* Hexahedra definition and storage
         * f = front b = back || u = up vs. d = down || l = left vs. r = right */

        int fdl,fdr,bdl,bdr,ful,fur,bul,bur;
        fdl = (k * imax * jmax) + (j * imax) + i + 1;
        fdr = fdl + 1;
        bdl = ((k+1) * imax * jmax) + (j * imax) + i + 1;
        bdr = bdl + 1;
        ful = (k * imax * jmax) + ((j+1) * imax) + i + 1;
        fur = ful + 1;
        bul = ((k+1) * imax * jmax) + ((j+1) * imax) + i + 1;
        bur = bul + 1;

        hexTab[hexTabPosition]   = fdl;
        hexTab[hexTabPosition+1] = fdr;
        hexTab[hexTabPosition+2] = bdr;
        hexTab[hexTabPosition+3] = bdl;
        hexTab[hexTabPosition+4] = ful;
        hexTab[hexTabPosition+5] = fur;
        hexTab[hexTabPosition+6] = bur;
        hexTab[hexTabPosition+7] = bul;
        hexTab[hexTabPosition+8] = ref;
      }
    }
  }


  /* Save he hexa mesh */
  FILE* inm = fopen("hexa.mesh","w");

  fprintf(inm,"MeshVersionFormatted 2\n");
  fprintf(inm,"\n\nDimension 3\n");
  fprintf(inm,"\n\nVertices\n");

  fprintf(inm,"%d\n",mmgMesh->np);

  for (int k=1; k<=mmgMesh->np; k++) {
    MMG5_pPoint ppt = &mmgMesh->point[k];
    fprintf(inm,"%.15lg %.15lg %.15lg %d\n",ppt->c[0],ppt->c[1],ppt->c[2],abs(ppt->ref));
  }

  fprintf(inm,"\n\nHexahedra %d\n",nbHex);
  for (int k=1; k<=nbHex; k++) {
    fprintf(inm,"%d %d %d %d %d %d %d %d %d\n",hexTab[9*k],hexTab[9*k+1],hexTab[9*k+2],
            hexTab[9*k+3],hexTab[9*k+4],hexTab[9*k+5],hexTab[9*k+6],hexTab[9*k+7],hexTab[9*k+8] );
  }

  fprintf(inm,"\n\nEnd\n");

  fclose(inm);

  /** Step 5: converts hexa into a MMG5 tetrahedral mesh */
  ier = H2T_libhex2tet(mmgMesh,hexTab,nbHex);

  if ( ier != H2T_STRONGFAILURE ) {
    MMG3D_saveMesh(mmgMesh,fileout);
  }
  else {
    printf("Hex2tet Fail: unable to save mesh.\n");
  }

  free(hexTab);

  int             ip,isCorner,isRequired;
  int             np,ne,nprism,nt,nquad,na;
  double          x, y, z, hmin, hmax;
 
  if ( MMG3D_Chk_meshData(mmgMesh,mmgSol) != 1 ) exit(EXIT_FAILURE);
  
  ier = MMG3D_Get_meshSize(mmgMesh,&np,&ne,&nprism,&nt,&nquad,&na);
  ier = MMG3D_Set_solSize(mmgMesh,mmgSol,MMG5_Vertex,np,MMG5_Scalar);

  hmin = 0.0015;
  hmax = 0.6;
  double Lx,Ly,f;
  for(ip=1;ip<=np;ip++){
    ier = MMG3D_Get_vertex(mmgMesh,&x,&y,&z,&ref,&isCorner,&isRequired);
    f = z-z0-A*cos(2.0*M_PI*kx*x)*cos(2.0*M_PI*ky*y);
    ier = MMG3D_Set_scalarSol(mmgSol,f,ip);
    if( reqTab[ip] ) ier = MMG3D_Set_requiredVertex(mmgMesh,ip);
  }

  free(reqTab);

  /** 1) Automatically save the mesh */
  if ( MMG3D_saveMesh(mmgMesh,"dom.in.mesh") != 1 ) {
    fprintf(stdout,"UNABLE TO SAVE MESH\n");
    return(MMG5_STRONGFAILURE);
  }

  /** 2) Automatically save the solution */
  if ( MMG3D_saveSol(mmgMesh,mmgSol,"dom.in.sol") != 1 ) {
    fprintf(stdout,"UNABLE TO SAVE SOL\n");
    return(MMG5_LOWFAILURE);
  }

  /** ------------------------------ STEP  II -------------------------- */
  /** remesh function */
  if( MMG3D_Set_iparameter(mmgMesh,mmgSol,MMG3D_IPARAM_iso,1) !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_dparameter(mmgMesh,mmgSol,MMG3D_DPARAM_hmin,0.05) !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_dparameter(mmgMesh,mmgSol,MMG3D_DPARAM_hmax,0.5) !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_dparameter(mmgMesh,mmgSol,MMG3D_DPARAM_hausd,0.001)  !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_dparameter(mmgMesh,mmgSol,MMG3D_DPARAM_hgrad,1.7)   !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_dparameter(mmgMesh,mmgSol,MMG3D_DPARAM_angleDetection,89.0)   !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_iparameter(mmgMesh,mmgSol,MMG3D_IPARAM_verbose,10) !=1 ) exit(EXIT_FAILURE);
 
  ier = MMG3D_mmg3dls(mmgMesh,mmgSol);

  if ( ier == MMG5_STRONGFAILURE ) {
    fprintf(stdout,"BAD ENDING OF MMG3DLIB: UNABLE TO SAVE MESH\n");
    return(ier);
  } else if ( ier == MMG5_LOWFAILURE )
    fprintf(stdout,"BAD ENDING OF MMG3DLIB\n");

  /** ------------------------------ STEP III -------------------------- */
  /** get results */
  /** Two solutions: just use the MMG3D_saveMesh/MMG3D_saveSol functions
      that will write .mesh(b)/.sol formatted files or manually get your mesh/sol
      using the MMG3D_getMesh/MMG3D_getSol functions */

  /** 1) Automatically save the mesh */
  if ( MMG3D_saveMesh(mmgMesh,"dom.o.mesh") != 1 ) {
    fprintf(stdout,"UNABLE TO SAVE MESH\n");
    return(MMG5_STRONGFAILURE);
  }

  /** 2) Automatically save the solution */
  if ( MMG3D_saveSol(mmgMesh,mmgSol,"dom.o.sol") != 1 ) {
    fprintf(stdout,"UNABLE TO SAVE SOL\n");
    return(MMG5_LOWFAILURE);
  }



  MMG3D_Free_all(MMG5_ARG_start,
                 MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
                 MMG5_ARG_end);

  return ier;
}
