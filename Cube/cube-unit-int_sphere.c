#include <stdio.h>
#include <stdlib.h>

#include "mmg/libmmg.h"
#define INPUT_MESH "cube-unit-coarse.mesh"
#define OUTPUT_SOL "cube-unit-coarse-int_sphere.sol"
#define OUTPUT_REMESH "cube-unit-int_sphere.mesh"
#define OUTPUT_RE_SOL "cube-unit-int_sphere.sol"

/* How to define an isotropic size map */

int main( void )
{
  MMG5_pMesh      mmgMesh;
  MMG5_pSol       mmgSol;
  double          x,y,z;
  int             k,ier;
  int             np,ntet,nprism,nt,nquad,na;

  /*initialisation of the mmg mesh*/
  mmgMesh = NULL;
  mmgSol  = NULL;

  MMG3D_Init_mesh(MMG5_ARG_start,
  		  MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
  		  MMG5_ARG_end);

  if ( MMG3D_Set_iparameter(mmgMesh, mmgSol, MMG3D_IPARAM_verbose, 5) != 1 ) return(0);

  /*lecture du maillage initial*/
  printf("\nMESH READING\n");
  if ( MMG3D_loadMesh(mmgMesh,INPUT_MESH) !=1 ) return(0);

  /*recuperation du nombre de points, triangles, aretes*/
  if ( MMG3D_Get_meshSize(mmgMesh,&np,&ntet,&nprism,&nt,&nquad,&na) != 1 )
    return(0);

  /*allocation of the sol data structure*/
  if ( MMG3D_Set_solSize(mmgMesh,mmgSol,MMG5_Vertex,np,MMG5_Scalar) != 1 ) return(0);

  /*impose size map*/
  int l = 0;
  for(k=1 ; k<=np ; k++) {
    if ( MMG3D_Get_vertex(mmgMesh,&x,&y,&z,NULL,NULL,NULL)!=1 )  return(0);
    
    printf( "\t\t\t(%f, %f, %f): %f \n", x, y, z, (x - 0.5) * (x - 0.5) + (y - 0.5) * (y - 0.5) + (z - 0.5) * (z - 0.5) );
    if( (x - 0.5) * (x - 0.5) + (y - 0.5) * (y - 0.5) + (z - 0.5) * (z - 0.5) < 0.04 ) {
      l++;
      if ( MMG3D_Set_scalarSol(mmgSol,0.02,k) != 1 ) return(0);
    } else {
      if ( MMG3D_Set_scalarSol(mmgSol,0.3,k) != 1 ) return(0);
    }
  }
  printf("\n\n\n \t\t\tFOUND %d internal points \n\n\n", l);
  printf("\nSOL SAVING\n");
  MMG3D_saveSol(mmgMesh,mmgSol,OUTPUT_SOL);

  /*call mmg3d lib*/
  ier = MMG3D_mmg3dlib(mmgMesh,mmgSol);
  if ( ier == MMG5_STRONGFAILURE ) {
    fprintf(stdout,"BAD ENDING OF MMG3DLIB: UNABLE TO SAVE MESH\n");
    return(0);
  } else if ( ier == MMG5_LOWFAILURE )
    fprintf(stdout,"BAD ENDING OF MMG3DLIB\n");

  /*save mesh and sol*/
  printf("\nMESH AND SOL SAVING\n");
  if ( MMG3D_saveMesh(mmgMesh,OUTPUT_REMESH) !=1 ) return(0);

  if ( MMG3D_saveSol(mmgMesh,mmgSol,OUTPUT_RE_SOL) !=1 ) return(0);

  /*free structure*/
  MMG3D_Free_all(MMG5_ARG_start,
		 MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
		 MMG5_ARG_end);
 
  return EXIT_SUCCESS;
}
