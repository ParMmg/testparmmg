Meshes of a unit cube [0,1] x [0,1] x [0,1]


Original mesh is cube-unit-coarse.mesh which contains:
  Vertices        152  Corners  8
  Triangles       300
  Tetrahedra      371
  Edges            60  Ridges   60
  Bounding box:  x:[0  1]  y:[0  1]  z:[0  1]


Using the two little programmes produced by the .c files and Makefile, one can
create the following test cases (used in pmmg ctests):

  A unit cube with two areas of different mesh density:
    half ( specifically for x > 0.5 ) of the cube is ~10 times more dense than
    the other half

    cube_unit-dual_density.mesh/.sol contains the remeshed mesh that mmg3d
    produces of size:
      Vertices      21121  Corners  25  Required 13
      Triangles      8584
      Tetrahedra   113641
      Edges           371  Ridges   254
      Normals        4035  Tangents   331
      Bounding box:  x:[0  1]  y:[0  1]  z:[0  1]
      Solutions     21121

    cube-unit-coarse-dual_density.sol contains the solution file to use with the
    original unit cube mesh ( cube-unit-coarse.mesh) .


  A unit cube with two areas of different mesh density:
    a sphere of radius ~0.2 in the center of the cube is ~10 times more dense
    than the rest of the cube.

    cube-unit-int_sphere.mesh/.sol  contains the remeshed mesh that mmg3d
    produces of size:
      Vertices       7939  Corners  25  Required 13
      Triangles       830
      Tetrahedra    46299
      Edges           132  Ridges   73
      Normals         339  Tangents   92
      Bounding box:  x:[0  1]  y:[0  1]  z:[0  1]
      Solutions      7939

    cube-unit-coarse-int_sphere.sol  contains the solution file to use with the
    original unit cube mesh ( cube-unit-coarse.mesh) .
