/* =============================================================================
**  This file is part of the mmg software package for the tetrahedral
**  mesh modification.
**  Copyright (c) Bx INP/Inria/UBordeaux/UPMC, 2004- .
**
**  mmg is free software: you can redistribute it and/or modify it
**  under the terms of the GNU Lesser General Public License as published
**  by the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  mmg is distributed in the hope that it will be useful, but WITHOUT
**  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
**  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
**  License for more details.
**
**  You should have received a copy of the GNU Lesser General Public
**  License and of the GNU General Public License along with mmg (in
**  files COPYING.LESSER and COPYING). If not, see
**  <http://www.gnu.org/licenses/>. Please read their terms carefully and
**  use this copy of the mmg distribution only if you accept them.
** =============================================================================
*/

/**
 * Example of use of the mmg3d library (basic use of mesh adaptation)
 *
 * \author Luca Cirrottola (Inria)
 *
 * \copyright GNU Lesser General Public License.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <float.h>

/** Include the mmg3d library hader file */
// if the header file is in the "include" directory
// #include "libmmg3d.h"
// if the header file is in "include/mmg/mmg3d"
#include "mmg/mmg3d/libmmg3d.h"
#include "libmmg3d_private.h"
#include "mmgcommon.h"

void radial_size(double *h,double hmin,double hmax,double rmin,double rmax,double x,double y,double xc,double yc,double c);

int main(int argc,char *argv[]) {
  MMG5_pMesh      mmgMesh,mmgMesh1;
  MMG5_pSol       mmgSol,mmgSol1;
  MMG5_Hash       hash;
  int             ier;
  int             nrefs,ref;
  int             k,k1,ip,ia,v0,v1,v2,v3,j,ip0,ip1,isCorner,isRidge,isRequired;
  int             np,ne,nprism,nt,nquad,na;
  int             *adjt;
  double          x, y, z, hmin, hmax, hmaxwing,h;
  char            *pwd,*filename;

  fprintf(stdout,"  -- TEST MMG3DLIB \n");

  /* Name and path of the mesh file */
  pwd = getenv("PWD");
  filename = (char *) calloc(strlen(pwd) + 58, sizeof(char));
  if ( filename == NULL ) {
    perror("  ## Memory problem: calloc");
    exit(EXIT_FAILURE);
  }
  sprintf(filename, "%s", "dom.in.msh");

  /** ------------------------------ STEP   I -------------------------- */
  /** 1) Initialisation of mesh and sol structures */
  /* args of InitMesh:
   * MMG5_ARG_start: we start to give the args of a variadic func
   * MMG5_ARG_ppMesh: next arg will be a pointer over a MMG5_pMesh
   * &mmgMesh: pointer toward your MMG5_pMesh (that store your mesh)
   * MMG5_ARG_ppMet: next arg will be a pointer over a MMG5_pSol storing a metric
   * &mmgSol: pointer toward your MMG5_pSol (that store your metric) */

  mmgMesh = NULL;
  mmgSol  = NULL;

  MMG3D_Init_mesh(MMG5_ARG_start,
                  MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
                  MMG5_ARG_end);

  /** 2) Build mesh in MMG5 format */
  /** Two solutions: just use the MMG3D_loadMesh function that will read a .mesh(b)
      file formatted or manually set your mesh using the MMG3D_Set* functions */

  /** with MMG3D_loadMesh function */
  if ( MMG3D_loadMshMesh(mmgMesh,mmgSol,filename) != 1 )  exit(EXIT_FAILURE);

  /** 3) Build sol in MMG5 format */
  /** Two solutions: just use the MMG3D_loadSol function that will read a .sol(b)
      file formatted or manually set your sol using the MMG3D_Set* functions */

//  /** With MMG3D_loadSol function */
//  if ( MMG3D_loadSol(mmgMesh,mmgSol,filename) != 1 )
//    exit(EXIT_FAILURE);

  /** 4) (not mandatory): check if the number of given entities match with mesh size */
  if ( MMG3D_Chk_meshData(mmgMesh,mmgSol) != 1 ) exit(EXIT_FAILURE);

  ier = MMG3D_Get_meshSize(mmgMesh,&np,&ne,&nprism,&nt,&nquad,&na);
//  ier = MMG3D_Set_solSize(mmgMesh,mmgSol,MMG5_Vertex,np,MMG5_Scalar);

  /* Count ref edges (warning: only for manifolds without open boundaries) */
  ier = MMG3D_hashTria(mmgMesh,&hash);
  na = 0;
  for( k = 1; k <= mmgMesh->nt; k++ ) {
    adjt = &mmgMesh->adjt[3*(k-1)+1];
    for( j= 0; j < 3; j++ ){
      k1 = adjt[j]/3;
      if( k1 > k  ) continue; /* count only once */
      if( ( !k1 ) || ( mmgMesh->tria[k].ref != mmgMesh->tria[k1].ref ) ) na++;
    }
  }
  printf("na %d\n", na);

  mmgMesh1 = NULL;
  mmgSol1  = NULL;
  MMG3D_Init_mesh(MMG5_ARG_start,
                  MMG5_ARG_ppMesh,&mmgMesh1,MMG5_ARG_ppMet,&mmgSol1,
                  MMG5_ARG_end);
  ier = MMG3D_Set_meshSize(mmgMesh1,np,ne,nprism,nt,nquad,na);

  /* Copy first mesh */
  for( ip = 1; ip <= np; ip++ ){
    ier = MMG3D_Get_vertex(mmgMesh, &x,&y,&z,&ref,&isCorner,&isRequired);
    ier = MMG3D_Set_vertex(mmgMesh1, x, y, z, ref, ip);
    if( isCorner )   ier = MMG3D_Set_corner( mmgMesh1, ip );
    if( isRequired ) ier = MMG3D_Set_requiredVertex( mmgMesh1, ip );
  }
  for( k =1; k <= nt; k++ ) {
    ier = MMG3D_Get_triangle(mmgMesh, &v0,&v1,&v2,&ref,&isRequired);
    ier = MMG3D_Set_triangle(mmgMesh1, v0, v1, v2, ref, k);
    if( isRequired ) ier = MMG3D_Set_requiredTriangle( mmgMesh1, k );
  }
  for( k =1; k <= ne; k++ ) {
    ier = MMG3D_Get_tetrahedron(mmgMesh, &v0,&v1,&v2,&v3,&ref,&isRequired);
    ier = MMG3D_Set_tetrahedron(mmgMesh1, v0, v1, v2, v3, ref, k);
    if( isRequired ) ier = MMG3D_Set_requiredTetrahedron( mmgMesh1, k );
  }


  ia = 0;
  for( k = 1; k <= mmgMesh->nt; k++ ) {
    adjt = &mmgMesh->adjt[3*(k-1)+1];
    for( j = 0; j < 3; j++ ){
      k1 = adjt[j]/3;
      if( k1 > k  ) continue; /* count only once */
      if( ( !k1 ) || ( mmgMesh->tria[k].ref != mmgMesh->tria[k1].ref ) ) {
        ip0 = mmgMesh->tria[k].v[MMG5_iprv2[j]];
        ip1 = mmgMesh->tria[k].v[MMG5_inxt2[j]];
        ier = MMG3D_Set_edge( mmgMesh1, ip0, ip1, mmgMesh->tria[k].ref,++ia);
        ier = MMG3D_Set_ridge( mmgMesh1, ia);
      }
    }
  }
  printf("ia %d na %d\n",ia,na);

  hmin = 0.005;
  hmax = 1.0;
  hmaxwing = 0.05;
//  for(ip=1;ip<=np;ip++){
//    h = 1.4;
//    ier = MMG3D_Get_vertex(mmgMesh,&x,&y,&z,&ref,&isCorner,&isRequired);
//    radial_size(&h,0.2, hmax,  1.0,4.0,x,y,0.5,0.0,sqrt(pow(0.6,2)-pow(0.15,2)));  // Elliptical distrib.
////    radial_size(&h,0.10,0.03,  0.1,1.0,x,y,0.5,0.0,sqrt(pow(0.6,2)-pow(0.15,2)));  // Elliptical distrib.
////    radial_size(&h,0.08,hmax, 0.02,4.0,x,y,0.0,0.0,0.0);  // Leading edge
////    radial_size(&h,0.2, hmax,0.005,4.0,x,y,1.0,0.0,0.0);  // Trailing edge
////    radial_size(&h,0.01,1.4,0.05,4.0,x,y,0.72,0.0,0.0); // Hinge
//    ier = MMG3D_Set_scalarSol(mmgSol,h,ip);
//  }
 
  /** 1) Automatically save the mesh */
  if ( MMG3D_saveMesh(mmgMesh1,"dom.in.mesh") != 1 ) {
    fprintf(stdout,"UNABLE TO SAVE MESH\n");
    return(MMG5_STRONGFAILURE);
  }

  /** 2) Automatically save the solution */
  if ( MMG3D_saveSol(mmgMesh1,mmgSol1,"dom.in.sol") != 1 ) {
    fprintf(stdout,"UNABLE TO SAVE SOL\n");
    return(MMG5_LOWFAILURE);
  }

  /** ------------------------------ STEP  II -------------------------- */
  /** remesh function */
  if( MMG3D_Set_dparameter(mmgMesh1,mmgSol1,MMG3D_DPARAM_hmin,hmin) !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_dparameter(mmgMesh1,mmgSol1,MMG3D_DPARAM_hmax,hmax) !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_dparameter(mmgMesh1,mmgSol1,MMG3D_DPARAM_hausd,0.05)  !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_dparameter(mmgMesh1,mmgSol1,MMG3D_DPARAM_hgrad,1.2)   !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_iparameter(mmgMesh1,mmgSol1,MMG3D_IPARAM_angle,1)   !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_dparameter(mmgMesh1,mmgSol1,MMG3D_DPARAM_angleDetection,120.0)   !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_iparameter(mmgMesh1,mmgSol1,MMG3D_IPARAM_numberOfLocalParam,2)   !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_localParameter(mmgMesh1,mmgSol1,MMG5_Triangle,3,hmin,hmaxwing,0.001) !=1 ) exit(EXIT_FAILURE);
  if( MMG3D_Set_localParameter(mmgMesh1,mmgSol1,MMG5_Triangle,4,hmin,hmaxwing,0.0005) !=1 ) exit(EXIT_FAILURE);
 
  ier = MMG3D_mmg3dlib(mmgMesh1,mmgSol1);

  if ( ier == MMG5_STRONGFAILURE ) {
    fprintf(stdout,"BAD ENDING OF MMG3DLIB: UNABLE TO SAVE MESH\n");
    return(ier);
  } else if ( ier == MMG5_LOWFAILURE )
    fprintf(stdout,"BAD ENDING OF MMG3DLIB\n");

  /** ------------------------------ STEP III -------------------------- */
  /** get results */
  /** Two solutions: just use the MMG3D_saveMesh/MMG3D_saveSol functions
      that will write .mesh(b)/.sol formatted files or manually get your mesh/sol
      using the MMG3D_getMesh/MMG3D_getSol functions */

  /** 1) Automatically save the mesh */
  if ( MMG3D_saveMesh(mmgMesh1,"dom.o.mesh") != 1 ) {
    fprintf(stdout,"UNABLE TO SAVE MESH\n");
    return(MMG5_STRONGFAILURE);
  }

  /** 2) Automatically save the solution */
  if ( MMG3D_saveSol(mmgMesh1,mmgSol1,"dom.o.sol") != 1 ) {
    fprintf(stdout,"UNABLE TO SAVE SOL\n");
    return(MMG5_LOWFAILURE);
  }


//  if ( MMG3D_saveMshMesh(mmgMesh1,NULL,"dom.o.msh") != 1 ) {
//    fprintf(stdout,"UNABLE TO SAVE MESH\n");
//    return(MMG5_STRONGFAILURE);
//  }
  FILE *inm;
  MMG5_pPoint ppt;
  MMG5_pTetra pt;
  MMG5_pTria  ptr;
  inm = fopen("dom.o.msh","w");
  fprintf(inm,"$MeshFormat\n2.2 0 8\n$EndMeshFormat\n");
  fprintf(inm,"$PhysicalNames\n3\n2 1 \"Farfield\"\n2 2 \"Wall\"\n2 3 \"Wing\"\n$EndPhysicalNames\n");
  fprintf(inm,"$Nodes\n%d\n",mmgMesh1->np);
  for( ip = 1; ip <= mmgMesh1->np; ip++ ) {
    ppt = &mmgMesh1->point[ip];
    fprintf(inm,"%d %.15lg %.15lg %.15lg\n",ip,ppt->c[0],ppt->c[1],ppt->c[2]);
  }
  fprintf(inm,"$EndNodes\n");
  fprintf(inm,"$Elements\n%d\n",mmgMesh1->nt+mmgMesh1->ne);
  for( k = 1; k <= mmgMesh1->nt; k++ ) {
    ptr = &mmgMesh1->tria[k];
    if( ptr->ref == 4 ) ptr->ref = 3; /* Merge wing surface and wing tip */
    fprintf(inm,"%d 2 2 %d %d %d %d %d\n",k,ptr->ref,ptr->ref,ptr->v[0],ptr->v[1],ptr->v[2]);
  }
  for( k = 1; k <= mmgMesh1->ne; k++ ) {
    pt = &mmgMesh1->tetra[k];
    pt->ref--; /* Decrease the volume reference */
    fprintf(inm,"%d 4 2 %d %d %d %d %d %d\n",k+mmgMesh1->nt,pt->ref,pt->ref,pt->v[0],pt->v[1],pt->v[2],pt->v[3]);
  }
  fprintf(inm,"$EndElements\n");
  fclose(inm);

  /** 3) Free the MMG3D5 structures */
  MMG3D_Free_all(MMG5_ARG_start,
                 MMG5_ARG_ppMesh,&mmgMesh,MMG5_ARG_ppMet,&mmgSol,
                 MMG5_ARG_end);
  MMG3D_Free_all(MMG5_ARG_start,
                 MMG5_ARG_ppMesh,&mmgMesh1,MMG5_ARG_ppMet,&mmgSol1,
                 MMG5_ARG_end);


  free(filename);
  filename = NULL;

  return(ier);
}

void radial_size(double *h,double hmin,double hmax,double rmin,double rmax,double x,double y,double xc,double yc,double c){
  double r,xl,xr,hnew;
  xl=xc-c;
  xr=xc+c;
  r=0.5*(sqrt(pow(x-xl,2)+pow(y-yc,2))+sqrt(pow(x-xr,2)+pow(y-yc,2)));
  if(r<rmin) hnew = hmin;
//  else if(r>rmax) hnew=hmax;
  else if(r>rmax) return;
  else hnew=hmin+(hmax-hmin)*(r-rmin)/(rmax-rmin); 
  // Apply only if leading to a refinement
  if(hnew < *h) *h = hnew;
  return;
}
