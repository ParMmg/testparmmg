SetFactory("OpenCASCADE");
Merge "M6.stp";

fscale = 0.001;
Affine { fscale, 0.0, 0.0, 0.0,   0.0, 0.0, -fscale, 0.0,   0.0, fscale, 0.0, 0.0 } {Volume{1};}

h = 0.01;
hmax = 100*h;
Characteristic Length {12, 6, 5, 7, 10, 4, 3, 1, 2, 9, 11, 8} = h;

R = 10;
ipc = newp; Point(ipc) = { 0.0, 0.0, 0.0, hmax };
ip0 = newp; Point(ip0) = { 0.0,  -R, 0.0, hmax };
ip1 = newp; Point(ip1) = {   R, 0.0, 0.0, hmax };
ip2 = newp; Point(ip2) = { 0.0,   R, 0.0, hmax };

ilc0 = newl;
Circle(ilc0) = { ip0, ipc, ip1 };
ilc1 = newl;
Circle(ilc1) = { ip1, ipc, ip2 };


ila = newl; Line(ila) = {ip2, ip0 };
ill = newll; Line Loop(ill) = {ilc0,ilc1,ila};
isc = news;
Surface(isc) = {ill};
V0[] = Extrude{ {0.0, R, 0.0},{0.0, R, 0.0}, -Pi  } {Surface{isc};};

V1[] = BooleanDifference {Volume{V0[1]}; Delete;} {Volume{1}; Delete;};

Printf("Volume: ",V1[]);
S1[] = Abs(Boundary{Volume{V1[]};});
Printf("Faces: ",S1[]);
// 0--1: farfield
// 2--3: wall
// 4--5: upper wing
// 6--7: lower wing
// 8--13: body of revolution
Physical Surface("Farfield") = {S1[0],S1[1]};
Physical Surface("Wall")     = {S1[2],S1[3]};
Physical Surface("Wing")     = {S1[4],S1[5],S1[6],S1[7]};
Physical Surface("WingTip")  = {S1[8],S1[9],S1[10],S1[11],S1[12],S1[13]};
Physical Volume(5)           = {V1[]};
