STP file from:
<https://turbmodels.larc.nasa.gov/Onerawingnumerics_grids/AileM6_with_sharp_TE.stp>

Build with gmsh 4.5.6 on Linux using:
gmsh -3 -format msh22 M6.geo
